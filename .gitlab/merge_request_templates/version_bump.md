Todo upon version bump:

For master:

 - [ ] update changelog: version and release date
 - [ ] merge release branch in Master
 - [ ] tag
 - [ ] push & tags

After tagging, commit prepare next release

 - [ ] merge back master into develop
 - [ ] update CHANGELOG.md: unreleased new version empty change
 - [ ] update README.md: bump apk stable version
 - [ ] close milestone
 - [ ] open next milestone
