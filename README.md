# TextLaunch

TextLaunch is simple and lightweight homescreen replacement for Android. It aims
at finding the App you want to launch as fast as possible. In order to reach
that goal it will show all your Apps sorted by:
1. Search query
2. Usage counter
3. Name

Here some showcase of what TextLaunch allows:

| Basic App search. | Search directly the web with your query pressing the search key on the softkeyboard. | Evaluate complex mathematical expressions without launching any app, using [this syntax](https://github.com/mariuszgromada/MathParser.org-mXparser#mxparser-in-nutshell). |
| :---------------------------------------         | :----------------------------------------    | :-----------------------------------          |
| ![Basic_workflow](doc/open_app_workflow.gif) | ![Search_web_workflow](doc/search_web_workflow.gif) | ![Expression_evaluation_workflow](doc/expression_evaluation_workflow.gif) |

It also allow basic app management like uninstall app though holding the app icon:
![App_management_workflow](doc/actions_for_apps_workflow.gif)

## Quick start user guide

TextLaunch is not available on the Google Play Store yet. Instead you will have
to manualy install the `.apk` file.

The first think to do is to download the right version on your mobile:

 - Get the [last stable version, v0.5](https://gitlab.com/MisterFruits/TextLaunch/-/jobs/533851755/artifacts/file/exported/TextLaunch-v0.5-stable.apk)
 - Navigate throught nightly release pipelines: Go [there](https://gitlab.com/MisterFruits/TextLaunch/pipelines?scope=branches&page=1)
   and go to the `package:debug` stage of the `develop` branch, then browse artifacts and and get the apk on the exported folder.

If you have troubles in installing the `apk` outsite the PlayStore, follow
[this steps](https://www.wikihow.tech/Install-APK-Files-on-Android).

## Contributing and feedback

Gitlab is the right place to feedback and contribute to the project.

Got a **question**? Somthing is **missing** in TextLaunch? Faced a **bug**?
Fill in a [new issue](https://gitlab.com/MisterFruits/TextLaunch/issues/new?issue)!

You can consult the [contributing guide](CONTRIBUTING.md) for more detailed
instructions.

## Quick start developer guide

Clone the repository and open the folder with your Android Studio instance.

You also have some custom gradle task:

 - `./gradlew clean gif` allows you to generate documentation gifs if you changed the UI
 - `./gradlew clean androidTest` run the integration tests that are not run on
   CI (see #18 for more infos)

## Credits:

 - Trash icon: https://material.io/resources/icons/?icon=delete_forever&style=baseline
 - Preference icon: https://material.io/resources/icons/?search=set&icon=settings&style=baseline
