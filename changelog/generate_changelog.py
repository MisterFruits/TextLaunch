import csv
import argparse
import time
from collections import namedtuple

def main():
    parser = argparse.ArgumentParser(description='Generate changelog entries from exported Gitlab issues (CSV file.')
    parser.add_argument('version', type=str,
                         help='Software version to be writen in changelog')
    parser.add_argument('csv', metavar='CVS', type=str,
                         help='the CSV file where issues have been exported from Gitlab')

    args = parser.parse_args()

    with open(args.csv, 'rb') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')

        print("# [%s] - %s" % (args.version, time.strftime("%Y-%m-%d")))
        for line in reader:
            print(" - %s - #%s" % (line['Title'], line['Issue ID']))


if __name__ == '__main__':
    main()
