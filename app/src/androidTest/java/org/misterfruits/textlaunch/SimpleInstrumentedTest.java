package org.misterfruits.textlaunch;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.EditText;

import androidx.test.uiautomator.UiDevice;

import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class SimpleInstrumentedTest {

    @Rule
    public ActivityTestRule<SearchActivity> mActivityRule =
            new ActivityTestRule<>(SearchActivity.class);


    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("org.misterfruits.textlaunch", appContext.getPackageName());
    }

    @Test
    public void checkAppIsDisplayed() {
        onView(withText("Contacts")).check(matches(isDisplayed()));
        onView(withId(R.id.search_query)).perform(typeText("qwz impossible search"));
        onView(withId(R.id.item_layout)).check(doesNotExist());
    }

    @Test
    public void checkExpressionEvaluationTextViewIsDisplayed() {
        onView(withId(R.id.search_query)).perform(typeText("4*5"));
        onView(withId(R.id.expression_text_view)).check(matches(isDisplayed()));
    }


    @Test
    public void test_onComingBackFromLaunchingApp_QueryEmpty(){
        onView(withId(R.id.search_query)).perform(typeText("contact"));
        onView(withText("Contacts")).perform(click());
        UiDevice mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        mDevice.pressBack();
        onView(isAssignableFrom(EditText.class)).check(matches(withText("")));
    }

    @Test
    public void test_onComingBackFromWebSearch_QueryEmpty() {
        ViewInteraction searchAutoComplete = onView(
                allOf(withClassName(is("android.widget.SearchView$SearchAutoComplete")),
                        AndroidTestHelper.childAtPosition(
                                allOf(withClassName(is("android.widget.LinearLayout")),
                                        AndroidTestHelper.childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                1)),
                                0),
                        isDisplayed()));
        searchAutoComplete.perform(replaceText("search"), closeSoftKeyboard());
        searchAutoComplete.perform(pressImeActionButton());
        UiDevice mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        mDevice.pressBack();
        ViewInteraction editText = onView(
                allOf(IsInstanceOf.<View>instanceOf(android.widget.EditText.class),
                        AndroidTestHelper.childAtPosition(
                                allOf(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                        AndroidTestHelper.childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class),
                                                1)),
                                0),
                        isDisplayed()));
        editText.check(matches(withText("")));
    }
}
