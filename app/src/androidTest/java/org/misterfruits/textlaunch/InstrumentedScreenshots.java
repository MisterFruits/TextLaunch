package org.misterfruits.textlaunch;

import android.graphics.Bitmap;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.runner.screenshot.ScreenCapture;
import android.support.test.runner.screenshot.Screenshot;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class InstrumentedScreenshots {

    @Rule
    public ActivityTestRule<SearchActivity> mActivityRule =
            new ActivityTestRule<>(SearchActivity.class);
    @Rule
    public final GrantPermissionRule mGrantPermissionRule =
            GrantPermissionRule.grant(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE);

    @Test
    public void generate_openAppWorkflow() throws InterruptedException {
        String screenshotName = "open_app_workflow";
        String textToSearch = "cnt";
        onView(withText("Contacts")).check(matches(isDisplayed()));
        onView(withId(R.id.search_query)).perform(typeText(""));
        Thread.sleep(1000);
        captureScreenshot(screenshotName + "-init");
        for (int i = 0; i < textToSearch.length() ; i++){
            onView(withId(R.id.search_query)).perform(typeText(String.valueOf(textToSearch.charAt(i))));
            Thread.sleep(100);
            captureScreenshot(String.format("%s-scene_%03d", screenshotName, (i)));
        }
        captureScreenshot(screenshotName + "-end_0");
        onView(withText("Contacts")).perform(click());
        Thread.sleep(1000);
        captureScreenshot(screenshotName + "-end_1");
    }

    @Test
    public void generate_searchWebWorkflow() throws InterruptedException {
        String screenshotName = "search_web_workflow";
        String textToSearch = "web search";
        ViewInteraction searchView = onView(
                allOf(withClassName(is("android.widget.SearchView$SearchAutoComplete")),
                        AndroidTestHelper.childAtPosition(
                                allOf(withClassName(is("android.widget.LinearLayout")),
                                        AndroidTestHelper.childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                1)),
                                0),
                        isDisplayed()));
        searchView.perform(replaceText(""));
        Thread.sleep(1000);
        captureScreenshot(screenshotName + "-init");
        for (int i = 0; i < textToSearch.length() ; i++){
            searchView.perform(replaceText(textToSearch.substring(0,i+1)));
            Thread.sleep(100);
            captureScreenshot(String.format("%s-scene_%03d", screenshotName, (i)));
        }
        captureScreenshot(screenshotName + "-end_0");
        searchView.perform(closeSoftKeyboard());
        searchView.perform(pressImeActionButton());
        Thread.sleep(2000);
        captureScreenshot(screenshotName + "-end_1");
        Thread.sleep(4000);
        captureScreenshot(screenshotName + "-end_2");
    }

    @Test
    public void generate_expressionEvaluationWorkflow() throws InterruptedException {
        String screenshotName = "expression_evaluation_workflow";
        String textToSearch = "e^(3+4)-e^3*e^4";
        onView(withId(R.id.search_query)).perform(typeText(""));
        Thread.sleep(1000);
        captureScreenshot(screenshotName + "-init");
        for (int i = 0; i < textToSearch.length() ; i++){
            onView(withId(R.id.search_query)).perform(typeText(String.valueOf(textToSearch.charAt(i))));
            Thread.sleep(100);
            captureScreenshot(String.format("%s-scene_%03d", screenshotName, (i)));
        }
        captureScreenshot(screenshotName + "-end_0");
    }

    @Test
    public void generate_actionForAppsWorkflow() throws InterruptedException {
        String screenshotName = "actions_for_apps_workflow";
        onView(withId(R.id.search_query)).perform(typeText(""));
        Thread.sleep(1000);
        captureScreenshot(screenshotName + "-init");
        onView(withText("Contacts")).perform(longClick());
        Thread.sleep(1000);
        captureScreenshot(String.format("%s-scene_%03d", screenshotName, (0)));
        captureScreenshot(screenshotName + "-end_0");
        onView(withText("Uninstall")).perform(click());
        Thread.sleep(1000);
        captureScreenshot(screenshotName + "-end_1");
    }


    private static void captureScreenshot(String name) {
        ScreenCapture capture = Screenshot.capture();
        capture.setFormat(Bitmap.CompressFormat.PNG);
        capture.setName(name);
        try {
            capture.process();
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }
}
