package org.misterfruits.textlaunch;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PackageInfoStructTest {

    @Test
    public void constructor() {
        String name = "com.toto";
        String label = "Toto";
        PackageInfoStruct struct = new PackageInfoStruct(name, label, null);
        assertEquals(name, struct.name);
        assertEquals(label, struct.label);
        assertEquals(null, struct.icon);
    }
}