package org.misterfruits.textlaunch;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.SearchView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
@Config
public class SearchActivityTest {

    SearchActivity activity;
    SearchView searchView;
    RecyclerView gridView;

    @Before
    public void setUp() {
        activity = Robolectric.setupActivity(SearchActivity.class);
        searchView = activity.findViewById(R.id.search_query);
        gridView = activity.findViewById(R.id.item_grid_view);
    }

    @Test
    public void test_getAppList_returnsActivities(){
        List<PackageInfoStruct> list = activity.getAppsList();
        assertEquals("There should be one (TextLaunch) app installed on roboelectric",1, list.size());
        assertEquals("On first launch app usage should be 0", 0, list.get(0).usageCounter);
    }

    @Test
    public void test_getAppList_hasPackageCounterUpdated(){
        List<PackageInfoStruct> list = activity.getAppsList();
        assertEquals("There should be one (TextLaunch) app installed on roboelectric",1, list.size());
        assertEquals("On first launch app usage should be 0", 0, list.get(0).usageCounter);
        activity.getSharedPreferences(activity.getString(R.string.package_usage_counter_file), Context.MODE_PRIVATE).edit().putInt(list.get(0).name.toString(), 345).commit();
        list = activity.getAppsList();
        assertEquals("There should be one (TextLaunch) app installed on roboelectric",1, list.size());
        assertEquals("On first launch app usage should be 345", 345, list.get(0).usageCounter);
    }

    @Test
    public void test_updateList_onSearchTextModified(){
        assertEquals(1, gridView.getChildCount());
        searchView.setQuery("zwz", false);
        assertEquals("This search query shouldn't match any app", 0, gridView.getChildCount());
    }

    @Test
    public void test_launchApp_onClick() {
        assertThat("Nothing should be in the stake for now", shadowOf(activity).getNextStartedActivity(), is(nullValue()));
        AppsAdapter.ItemViewHolder itemView = (AppsAdapter.ItemViewHolder) gridView.findViewHolderForAdapterPosition(0);
        itemView.onClick(gridView);
        assertThat("Should have launch an app", shadowOf(activity).getNextStartedActivity(), is(not(nullValue())));
    }

    @Test
    public void test_launchInexistantApp_doesntCrashAndReloadApps_onClick(){
        PackageInfoStruct inexistant = new PackageInfoStruct("toto.tata.tutu", "label", null);
        activity.allApps.add(inexistant);
        assertTrue("All apps should contain the inexistant app before", activity.allApps.contains(inexistant));
        activity.onClickItem(inexistant);
        assertFalse("All apps shouldn't contain the inexistant app", activity.allApps.contains(inexistant));
    }

    @Test
    public void test_cleanQuery_onClick() {
        assertThat("Nothing should be in the stake for now", shadowOf(activity).getNextStartedActivity(), is(nullValue()));
        searchView.setQuery("tl", false);
        AppsAdapter.ItemViewHolder itemView = (AppsAdapter.ItemViewHolder) gridView.findViewHolderForAdapterPosition(0);
        itemView.onClick(gridView);
        assertThat("Query shoud be reinitialised", searchView.getQuery().toString(), is(""));
    }

    @Test
    public void test_updatePackageUsageCounter_onClick() {
        AppsAdapter.ItemViewHolder itemView = (AppsAdapter.ItemViewHolder) gridView.findViewHolderForAdapterPosition(0);
        PackageInfoStruct item = activity.filteredAndSortedApps.get(itemView.getAdapterPosition());
        int previousCounter = item.usageCounter;
        assertThat("Nothing should be in the stake for now", shadowOf(activity).getNextStartedActivity(), is(nullValue()));
        itemView.onClick(gridView);
        assertEquals("Package info usage counter should have been updated + 1", previousCounter + 1, item.usageCounter);
    }

    @Test
    public void test_updatePackageUsageCounter_whileSharedPrefUsageChanged_onPause() {
        AppsAdapter.ItemViewHolder itemView1 = (AppsAdapter.ItemViewHolder) gridView.findViewHolderForAdapterPosition(0);
        PackageInfoStruct item1 = activity.filteredAndSortedApps.get(itemView1.getAdapterPosition());
        SharedPreferences pref = activity.getSharedPreferences(activity.getString(R.string.package_usage_counter_file), Context.MODE_PRIVATE);
        int previousCounter = item1.usageCounter;
        assertEquals("Package info usage counter should be consistant with shared pref", previousCounter, pref.getInt(item1.name.toString(), 0));
        activity.onPause();
        pref.edit().putInt(item1.name.toString(), previousCounter + 1).commit();
        activity.onResume();
        AppsAdapter.ItemViewHolder itemView2 = (AppsAdapter.ItemViewHolder) gridView.findViewHolderForAdapterPosition(0);
        PackageInfoStruct item2 = activity.filteredAndSortedApps.get(itemView2.getAdapterPosition());
        assertEquals("We're testing the same package", item1.name, item2.name);
        assertEquals("Shared preference usage counter should have been updated after paused", previousCounter + 1, item2.usageCounter);
    }

    @Test
    public void test_updateSharedPrefUsageCounter_onPause() {
        AppsAdapter.ItemViewHolder itemView = (AppsAdapter.ItemViewHolder) gridView.findViewHolderForAdapterPosition(0);
        PackageInfoStruct item = activity.filteredAndSortedApps.get(itemView.getAdapterPosition());
        SharedPreferences pref = activity.getSharedPreferences(activity.getString(R.string.package_usage_counter_file), Context.MODE_PRIVATE);
        int previousCounter = item.usageCounter;
        assertEquals("Package info usage counter should be consistant with shared pref", previousCounter, pref.getInt(item.name.toString(), 0));
        assertThat("Nothing should be in the stake for now", shadowOf(activity).getNextStartedActivity(), is(nullValue()));
        itemView.onClick(gridView);
        assertEquals("Shared preference usage counter should remain untouched until app paused", previousCounter, pref.getInt(item.name.toString(), 0));
        assertEquals("Package info usage counter should have been updated + 1", previousCounter + 1, item.usageCounter);
        activity.onPause();
        assertEquals("Shared preference usage counter should have been updated after paused", previousCounter + 1, pref.getInt(item.name.toString(), 0));
    }

    @Test
    public void test_searchWeb_onQueryTestSubmit(){
        searchView.setQuery("", true);
        assertNull("Submitting empty query should not launch an activity", shadowOf(activity).getNextStartedActivity());
        searchView.setQuery("test", true);
        Intent i = shadowOf(activity).getNextStartedActivity();
        assertEquals("Submitting a query should launch an activity", i.getAction(), Intent.ACTION_WEB_SEARCH);
    }

    @Test
    public void test_uninstallIntent_onUninstallButtonClick(){
        activity.onLongClickItem(new PackageInfoStruct("com.toto", "toto", null), searchView);
        View uninstallButton = activity.popupWindow.getContentView().findViewById(R.id.popup_uninstall_button);
        uninstallButton.callOnClick();
        Intent nextActivity = shadowOf(activity).getNextStartedActivity();
        assertNotNull("Clicking uninstall should launch an uninstall activity", nextActivity);
        assertEquals("Intent action should be to uninstall", nextActivity.getAction(), Intent.ACTION_UNINSTALL_PACKAGE);
        assertEquals("Intent action should be to uninstall", nextActivity.getData().toString(), "package:com.toto");
    }
}


