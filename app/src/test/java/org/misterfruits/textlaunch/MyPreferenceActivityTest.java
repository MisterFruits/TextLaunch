package org.misterfruits.textlaunch;

import android.content.Context;
import android.content.SharedPreferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.robolectric.util.FragmentTestUtil.startFragment;

@RunWith(RobolectricTestRunner.class)
@Config
public class MyPreferenceActivityTest {

    MyPreferenceActivity activity;
    MyPreferenceActivity.MyPreferenceFragment prefFragment;
    SharedPreferences packageUsageCounter;

    @Before
    public void setUp() {
        activity = Robolectric.setupActivity(MyPreferenceActivity.class);
        prefFragment = new MyPreferenceActivity.MyPreferenceFragment();
        packageUsageCounter = activity.getSharedPreferences(activity.getString(R.string.package_usage_counter_file), Context.MODE_PRIVATE);
        prefFragment.setPackageUsageCounter(packageUsageCounter);
        startFragment(prefFragment);
    }

    @Test
    public void test_clearSharedPreferences_onResetAppUsageBtn(){
        packageUsageCounter.edit().putInt("toto", 1).commit();
        assertFalse("packageUsageCounter should not be empty", packageUsageCounter.getAll().isEmpty());

        prefFragment.findPreference("reset_app_counter_button").getOnPreferenceClickListener().onPreferenceClick(null);
        assertTrue("packageUsageCounter should be empty after reset app btn pressed", packageUsageCounter.getAll().isEmpty());

    }
}
