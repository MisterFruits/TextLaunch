package org.misterfruits.textlaunch.search;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SimpleFuzzySearchTest {

    @Test
    public void test_getMatch() {
        SimpleFuzzySearch sfs = new SimpleFuzzySearch();
        Match m = sfs.getMatch("pp", "papa");
        assertTrue(m.matched);
        assertEquals(0, m.rank);
        assertEquals(new Integer(0), m.matchedIndices.get(0));
        assertEquals(new Integer(2), m.matchedIndices.get(1));

        m = sfs.getMatch("pp", "abricot");
        assertFalse(m.matched);
        assertEquals(0, m.rank);
        assertTrue(m.matchedIndices.isEmpty());
    }

    @Test
    public void test_getMatchWithAccents() {
        SimpleFuzzySearch sfs = new SimpleFuzzySearch();
        Match m = sfs.getMatch("meteo", "Météo");
        assertTrue(m.matched);
        assertEquals(0, m.rank);
        assertEquals(new Integer(0), m.matchedIndices.get(0));
        assertEquals(new Integer(1), m.matchedIndices.get(1));
        assertEquals(new Integer(2), m.matchedIndices.get(2));
        assertEquals(new Integer(3), m.matchedIndices.get(3));
        assertEquals(new Integer(4), m.matchedIndices.get(4));
    }
}