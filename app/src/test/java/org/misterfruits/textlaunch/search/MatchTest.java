package org.misterfruits.textlaunch.search;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class MatchTest {

    @Test
    public void compareTo_nullMatch() {
        Match m = new Match(true, 1, null);
        assertTrue(m.compareTo(null) > 0);
        m.matched = false;
        assertTrue(m.compareTo(null) > 0);
        m.rank = -1;
        assertTrue(m.compareTo(null) > 0);
    }

    @Test
    public void compareTo_sameRank(){
        Match m = new Match(false, 200, null);
        Match n = new Match(true, 200, null);
        assertTrue(m.compareTo(n) < 0);
        assertTrue(n.compareTo(m) > 0);
    }

    @Test
    public void compareTo_rank(){
        Match m = new Match(false, 300, null);
        Match n = new Match(true, 200, null);
        assertTrue(m.compareTo(n) > 0);
        m.matched = true;
        assertTrue(m.compareTo(n) > 0);
        n.matched = false;
        assertTrue(m.compareTo(n) > 0);
    }
}