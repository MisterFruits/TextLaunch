package org.misterfruits.textlaunch.search;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UtilsTest {
    @Test
    public void test_deAccent() {
        String origin   = "ā, ī, ū), over- and underdots (ṛ, ḥ, ṃ, ṇ, ṣ, ṭ, ḍā, ī, ū), over- and underdots (ṛ, ḥ, ṃ, ṇ, ṣ, ṭ, ḍ, à, á, â, ã, ç, é, ê, í, ó, ô, õ, ú, á, à, ç, é, è, í, ï, ó, ò, ú, ü, n·h, s·h";
        String expected = "a, i, u), over- and underdots (r, h, m, n, s, t, da, i, u), over- and underdots (r, h, m, n, s, t, d, a, a, a, a, c, e, e, i, o, o, o, u, a, a, c, e, e, i, i, o, o, u, u, n·h, s·h";
        assertEquals(expected, Utils.deAccent(origin));
    }

    @Test
    public void test_normalize() {
        String origin = "Salut c'est càal";
        String expected = "salut c'est caal";
        assertEquals(expected, Utils.normalize(origin));

    }
}
