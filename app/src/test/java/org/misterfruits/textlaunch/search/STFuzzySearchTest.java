package org.misterfruits.textlaunch.search;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class STFuzzySearchTest {

    List<String> db1 = Arrays.asList(new String[]{"papa", "abricot"});
    List<String> db2 = Arrays.asList(new String[]{"Contact", "Chrome"});
    STFuzzySearch fs;

    @Before
    public void setup() {
        fs = new STFuzzySearch();
        assertNotNull("Initialisation failed", fs);
    }

    private List<Match> getMatches(CharSequence searchPatern, List<String> db){
        List<Match> res = new ArrayList<Match>();
        for (String item: db) {
            res.add(fs.getMatch(searchPatern, item));
        }
        return res;
    }

    @Test
    public void onSearch_testGetRank(){
        CharSequence search = "pa";
        List<Match> matches = getMatches(search, db1);
        assertTrue(String.format("'%s'(%d) should be ranked higher than '%s'(%d) with the search '%s'",
                db1.get(0), matches.get(0).rank, db1.get(1), matches.get(1).rank, search),
                matches.get(0).rank > matches.get(1).rank);
    }

    @Test
    public void onSearch_testMatched(){
        CharSequence search = "pa";
        List<Match> matches = getMatches(search, db1);
        assertTrue(String.format("'%s'(%b) should match with the search '%s'",
                db1.get(0), matches.get(0).matched, search),
                matches.get(0).matched);
        assertFalse(String.format("'%s'(%b) should not match with the search '%s'",
                db1.get(1), matches.get(1).matched, search),
                matches.get(1).matched);
    }

    @Test
    public void onSearch_testGetMatchedChar(){
        CharSequence search = "pp";
        List<Match> matches = getMatches(search, db1);
        assertEquals(String.format("'%s' should match 2 letters of search '%s'",
                db1.get(0), search),
                2, matches.get(0).matchedIndices.size());
        assertEquals(String.format("'%s' should match letter of index 0 of search '%s'",
                db1.get(0), search),
                0, (int)matches.get(0).matchedIndices.get(0));
        assertEquals(String.format("'%s' should match letter of index 2 of search '%s'",
                db1.get(0), search),
                2, (int)matches.get(0).matchedIndices.get(1));
        assertEquals(String.format("'%s' should match not letters with the search '%s'",
                db1.get(1), search),
                0, matches.get(1).matchedIndices.size());
    }

    @Test
    public void onSearch_emptyQueryRanks0(){
        CharSequence search = "";
        List<Match> matches = getMatches(search, db1);
        for(int i=0 ; i < db1.size() ; i++){
            assertEquals(String.format("Empty search on '%s' should be 0", db1.get(i)), 0, matches.get(i).rank);
        }
    }


    @Test
    public void onSearch_testGetTestRankBothMatchingChar(){
        CharSequence search = "co";
        List<Match> matches = getMatches(search, db2);
        assertTrue(String.format("'%s'(%d) should be ranked higher than '%s'(%d) with the search '%s'",
                db2.get(0), matches.get(0).rank, db2.get(1), matches.get(1).rank, search),
                matches.get(0).rank > matches.get(1).rank);
    }

    @Test
    public void test_getMatchWithAccents() {
        ISearch sfs = new STFuzzySearch();
        Match m = sfs.getMatch("meteo", "Météo");
        assertTrue("Search 'meteo' should match world 'Météo'", m.matched);
        assertTrue(m.rank > 0);
        assertEquals(new Integer(0), m.matchedIndices.get(0));
        assertEquals(new Integer(1), m.matchedIndices.get(1));
        assertEquals(new Integer(2), m.matchedIndices.get(2));
        assertEquals(new Integer(3), m.matchedIndices.get(3));
        assertEquals(new Integer(4), m.matchedIndices.get(4));
    }
}
