package org.misterfruits.textlaunch;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AppsAdapterTest {

    private List<PackageInfoStruct> sampleAppList;

    @Before
    public void setup(){
        sampleAppList = new ArrayList<PackageInfoStruct>();
        sampleAppList.add(new PackageInfoStruct("org.toto", "Toto", null));
        sampleAppList.add(new PackageInfoStruct("org.tata", "Tata", null));
        sampleAppList.add(new PackageInfoStruct("com.foo", "Foo", null));
        sampleAppList.add(new PackageInfoStruct("org.meteo", "Météo", null));
    }

    @Test
    public void onCreation_testGetCount() {
        AppsAdapter adapter = new AppsAdapter(sampleAppList, null);
        assertEquals("On creation with no search, adapter should show all app", sampleAppList.size(), adapter.getItemCount());
        sampleAppList.add(new PackageInfoStruct("org.salut", "Salut", null));
        assertEquals("On list update, adapter be updated with all app", sampleAppList.size(), adapter.getItemCount());
    }

}