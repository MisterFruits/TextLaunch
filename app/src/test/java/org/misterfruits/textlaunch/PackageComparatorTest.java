package org.misterfruits.textlaunch;

import org.junit.Test;
import org.misterfruits.textlaunch.search.Match;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PackageComparatorTest {

    @Test
    public void testCompare() {
        PackageInfoStruct p = new PackageInfoStruct("foo", "fool", null, new Match(true, 100, null));
        PackageInfoStruct q = new PackageInfoStruct("bar", "barl", null, null);
        List<PackageInfoStruct> test = new ArrayList<PackageInfoStruct>();
        test.add(q);
        test.add(p);
        test.sort(new PackageComparator());
        assertEquals(p, test.get(0));
        q.match = new Match(true, 101, null);
        test.sort(new PackageComparator());
        assertEquals(q, test.get(0));
     }
}