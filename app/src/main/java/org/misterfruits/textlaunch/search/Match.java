package org.misterfruits.textlaunch.search;

import java.util.List;

public class Match implements Comparable<Match> {
    public boolean isMatched() {
        return matched;
    }

    public int getRank() {
        return rank;
    }

    public List<Integer> getMatchedIndices() {
        return matchedIndices;
    }

    protected boolean matched;
    protected int rank;
    protected List<Integer> matchedIndices;

    public Match(boolean matched, int rank, List<Integer> formattedStr){
        this.matched = matched;
        this.rank = rank;
        this.matchedIndices = formattedStr;
    }

    @Override
    public int compareTo(Match o) {
        if(o == null)
            return 1;
        if(rank == o.rank)
            return ((Boolean) matched).compareTo((Boolean) o.matched);
        return ((Integer) rank).compareTo((Integer) o.rank);
    }
}
