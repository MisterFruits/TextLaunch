package org.misterfruits.textlaunch.search;

import java.text.Normalizer;
import java.util.Locale;
import java.util.regex.Pattern;

public class Utils {
    public static CharSequence deAccent(CharSequence str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

    public static CharSequence normalize(CharSequence str) {
        return deAccent(str).toString().toLowerCase(Locale.getDefault());
    }

}
