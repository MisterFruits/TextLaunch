package org.misterfruits.textlaunch.search;

import java.util.ArrayList;
import java.util.List;

import static org.misterfruits.textlaunch.search.Utils.normalize;

public class SimpleFuzzySearch implements ISearch {
    @Override
    public Match getMatch(CharSequence patternStr, CharSequence strStr) {
        CharSequence pattern = normalize(patternStr);
        CharSequence str = normalize(strStr);
        List<Integer> matchedIndices = new ArrayList<Integer>();
        int patternInd = 0;
        int strInd = 0;
        while (patternInd < pattern.length() && strInd < str.length()){
            if (pattern.charAt(patternInd) == str.charAt(strInd)){
                ++patternInd;
                matchedIndices.add(strInd);
            }
            ++strInd;
        }
        return new Match(patternInd == pattern.length() ? true : false, 0, matchedIndices);
    }
}
