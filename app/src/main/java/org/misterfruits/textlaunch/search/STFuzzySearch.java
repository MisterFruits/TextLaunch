package org.misterfruits.textlaunch.search;

import java.util.ArrayList;

public class STFuzzySearch implements ISearch{
    // Score consts
    private int adjacency_bonus = 5;                // bonus for adjacent matches
    private int separator_bonus = 10;               // bonus if match occurs after a separator
    private int camel_bonus = 10;                   // bonus if match is uppercase and prev is lower
    private int leading_letter_penalty = -3;        // penalty applied for every letter in str before the first match
    private int max_leading_letter_penalty = -9;    // maximum penalty for leading letters
    private int unmatched_letter_penalty = 0;       // penalty for every letter that doesn't matter

    @Override
    public Match getMatch(CharSequence pattern, CharSequence strAccentued) {
        Match result = new Match(false, 0, new ArrayList<Integer>());
        CharSequence str = Utils.deAccent(strAccentued);
        // Loop variables
        int patternIdx = 0;
        int patternLength = pattern.length();
        int strIdx = 0;
        int strLength = str.length();
        boolean prevMatched = false;
        boolean prevLower = false;
        boolean prevSeparator = true;       // true so if first letter match gets separator bonus

        // Use "best" matched letter if multiple string letters match the pattern
        Character bestLetter = null;
        Character bestLower = null;
        Integer bestLetterIdx = null;
        int bestLetterScore = 0;

        // Loop over strings
        while (strIdx != strLength) {
            Character patternChar = patternIdx != patternLength ? pattern.charAt(patternIdx) : null;
            Character strChar = str.charAt(strIdx);

            Character patternLower = patternChar != null ? Character.toLowerCase(patternChar) : null;
            Character strLower = Character.toLowerCase(strChar);
            Character strUpper = Character.toUpperCase(strChar);

            boolean nextMatch = patternChar != null && patternLower == strLower;
            boolean rematch = bestLetter != null && bestLower == strLower;

            boolean advanced = nextMatch && bestLetter != null;
            boolean patternRepeat = bestLetter !=null && patternChar != null && bestLower == patternLower;
            if (advanced || patternRepeat) {
                result.rank += bestLetterScore;
                result.matchedIndices.add(bestLetterIdx);
                bestLetter = null;
                bestLower = null;
                bestLetterIdx = null;
                bestLetterScore = 0;
            }

            if (nextMatch || rematch) {
                int newScore = 0;

                // Apply penalty for each letter before the first pattern match
                // Note: std::max because penalties are negative values. So max is smallest penalty.
                if (patternIdx == 0) {
                    int penalty = Math.max(strIdx * leading_letter_penalty, max_leading_letter_penalty);
                    result.rank += penalty;
                }

                // Apply bonus for consecutive bonuses
                if (prevMatched)
                    newScore += adjacency_bonus;

                // Apply bonus for matches after a separator
                if (prevSeparator)
                    newScore += separator_bonus;

                // Apply bonus across camel case boundaries. Includes "clever" isLetter check.
                if (prevLower && strChar == strUpper && strLower != strUpper)
                    newScore += camel_bonus;

                // Update patter index IFF the next pattern letter was matched
                if (nextMatch)
                    ++patternIdx;

                // Update best letter in str which may be for a "next" letter or a "rematch"
                if (newScore >= bestLetterScore) {

                    // Apply penalty for now skipped letter
                    if (bestLetter != null)
                        result.rank += unmatched_letter_penalty;

                    bestLetter = strChar;
                    bestLower = Character.toLowerCase(bestLetter);
                    bestLetterIdx = strIdx;
                    bestLetterScore = newScore;
                }

                prevMatched = true;
            }
            else {
                result.rank += unmatched_letter_penalty;
                prevMatched = false;
            }

            // Includes "clever" isLetter check.
            prevLower = strChar == strLower && strLower != strUpper;
            prevSeparator = strChar == '_' || strChar == ' ';

            ++strIdx;
        }

        // Apply score for last match
        if (bestLetter != null) {
            result.rank += bestLetterScore;
            result.matchedIndices.add(bestLetterIdx);
        }

        result.matched = patternIdx == patternLength;
        return result;
    }
}
