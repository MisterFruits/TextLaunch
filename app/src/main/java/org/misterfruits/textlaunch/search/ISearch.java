package org.misterfruits.textlaunch.search;

public interface ISearch {
    Match getMatch(CharSequence searchPattern, CharSequence str);
}
