package org.misterfruits.textlaunch;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.SearchView;
import android.widget.TextView;

import org.mariuszgromada.math.mxparser.Expression;
import org.misterfruits.textlaunch.search.ISearch;
import org.misterfruits.textlaunch.search.STFuzzySearch;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class SearchActivity extends AppCompatActivity {

    AppsAdapter appsAdapter;
    List<PackageInfoStruct> allApps;
    List<PackageInfoStruct> filteredAndSortedApps;
    SharedPreferences packageUsageCounter;
    SharedPreferences appPreferences;
    ISearch search = new STFuzzySearch();
    Comparator<PackageInfoStruct> comparator = new PackageComparator();
    SearchView inputSearch;
    PopupWindow popupWindow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);

        packageUsageCounter = this.getSharedPreferences(getString(R.string.package_usage_counter_file), Context.MODE_PRIVATE);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        appPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        allApps = new ArrayList<>();
        filteredAndSortedApps = new ArrayList<>();
        appsAdapter = new AppsAdapter(filteredAndSortedApps, this);

        registerBroadcastApplicationChanges();

        final RecyclerView recyclerView = findViewById(R.id.item_grid_view);
        GridLayoutManager  mLayoutManager = new GridLayoutManager(this, 5);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(appsAdapter);

        final TextView expressionTextView = findViewById(R.id.expression_text_view);
        createPopupWindow(recyclerView);

        ImageButton preferenceBtn = findViewById(R.id.preference_button);
        preferenceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SearchActivity.this, MyPreferenceActivity.class);
                startActivity(i);
            }
        });

        inputSearch = findViewById(R.id.search_query);
        inputSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                try {
                    Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                    intent.putExtra(SearchManager.QUERY, query); // query contains search string
                    startActivity(intent);
                    inputSearch.setQuery("",false);
                } catch (ActivityNotFoundException e) {
                    new AlertDialog.Builder(SearchActivity.this)
                            .setTitle("Unable to handle your web search")
                            .setMessage(e.getMessage())
                            .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {}
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
                return true;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                Expression numericalExpression = new Expression(newText);
                if(numericalExpression.checkSyntax()){
                    double d = numericalExpression.calculate();
                    String display = String.format(Locale.getDefault(), "= %f", d);
                    expressionTextView.setText(display);
                    expressionTextView.setVisibility(View.VISIBLE);
                } else {
                    expressionTextView.setVisibility(View.GONE);
                }
                SearchActivity.this.applySearch(newText);
                return true;
            }
        });
        inputSearch.requestFocus();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveUsageToSharedPreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadAppList();
    }

    private void saveUsageToSharedPreferences() {
        SharedPreferences.Editor editor = packageUsageCounter.edit();
        for (PackageInfoStruct item: allApps) {
            editor.putInt(item.name.toString(), item.usageCounter);
        }
        editor.apply();
    }

    private void createPopupWindow(ViewGroup itemListView){
        LinearLayout popupLayout =  (LinearLayout) LayoutInflater.from(this).inflate(R.layout.popup_layout, itemListView,false);
        this.popupWindow = new PopupWindow(popupLayout, LinearLayout.LayoutParams.WRAP_CONTENT,
                                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                              true);
        popupLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }

    private void registerBroadcastApplicationChanges() {
        BroadcastReceiver packageInfoBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                saveUsageToSharedPreferences();
                reloadAppList();
            }
        };
        IntentFilter appChangedFilter = new IntentFilter(Intent.ACTION_PACKAGE_ADDED);
        appChangedFilter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        appChangedFilter.addAction(Intent.ACTION_PACKAGE_CHANGED);
        appChangedFilter.addDataScheme("package");
        SearchActivity.this.registerReceiver(packageInfoBroadcastReceiver, appChangedFilter);
    }

    private void applySearch(String newText) {
        filteredAndSortedApps.clear();
        for(PackageInfoStruct item: allApps){
            item.match = search.getMatch(newText, item.label);
            if(item.match.isMatched() || ! appPreferences.getBoolean("hide_unmatched", false)){
                filteredAndSortedApps.add(item);
            }
        }
        filteredAndSortedApps.sort(comparator);
        appsAdapter.notifyDataSetChanged();
    }

    protected List<PackageInfoStruct> getAppsList(){
        PackageManager manager = this.getPackageManager();

        List<PackageInfoStruct> result = new ArrayList<PackageInfoStruct>();

        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        for(ResolveInfo ri:manager.queryIntentActivities(i, 0)){
            PackageInfoStruct app = new PackageInfoStruct();
            app.label = ri.loadLabel(manager);
            app.icon = ri.activityInfo.loadIcon(manager);
            app.name = ri.activityInfo.packageName;
            app.usageCounter = packageUsageCounter.getInt(app.name.toString(), 0);
            result.add(app);
        }
        return result;
    }

    public void onClickItem(PackageInfoStruct item){
        Intent i = this.getPackageManager().getLaunchIntentForPackage(item.name.toString());
        if(i != null) {
            this.startActivity(i);
            inputSearch.setQuery("", false);
            item.usageCounter++;
        } else {
            new AlertDialog.Builder(SearchActivity.this)
                    .setTitle(String.format(Locale.getDefault(), getString(R.string.package_not_found_alert_title), item.label.toString()))
                    .setMessage(String.format(Locale.getDefault(), getString(R.string.package_not_found_alert_message), item.label.toString(), item.name.toString()))
                    .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {}
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            saveUsageToSharedPreferences();
            reloadAppList();
        }
    }

    public void onLongClickItem(final PackageInfoStruct item, View itemAnchorView) {
        TextView titleView = popupWindow.getContentView().findViewById(R.id.popup_title_text);
        titleView.setText(String.format(Locale.getDefault(), getString(R.string.popup_title_text), item.label.toString()));
        TextView deleteButton = popupWindow.getContentView().findViewById(R.id.popup_uninstall_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri packageURI = Uri.parse("package:"+item.name);
                Intent uninstallIntent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageURI);
                startActivity(uninstallIntent);
                popupWindow.dismiss();
            }
        });
        this.popupWindow.showAsDropDown(itemAnchorView, 0, - (int)(itemAnchorView.getMeasuredHeight()*1.5));
        this.popupWindow.update();
    }


    private void reloadAppList() {
        allApps.clear();
        allApps.addAll(getAppsList());
        applySearch(inputSearch.getQuery().toString());
    }

}
