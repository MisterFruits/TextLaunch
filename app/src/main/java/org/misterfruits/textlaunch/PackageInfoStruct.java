package org.misterfruits.textlaunch;

import android.graphics.drawable.Drawable;

import org.misterfruits.textlaunch.search.Match;

public class PackageInfoStruct {
    public CharSequence label;
    public Drawable icon;
    public CharSequence name;
    public Match match;
    public int usageCounter;

    public PackageInfoStruct(){
        this(null,null, null);
    }

    public PackageInfoStruct(CharSequence name, CharSequence label, Drawable icon){
        this(name, label, icon, null);
    }
    public PackageInfoStruct(CharSequence name, CharSequence label, Drawable icon, Match match){
        this(name, label, icon, match, 0);
    }
    public PackageInfoStruct(CharSequence name, CharSequence label, Drawable icon, Match match, int usageCounter) {
        this.name = name;
        this.label = label;
        this.icon = icon;
        this.match = match;
        this.usageCounter = usageCounter;
    }
}
