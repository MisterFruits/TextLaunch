package org.misterfruits.textlaunch;

import java.util.Comparator;

public class PackageComparator implements Comparator<PackageInfoStruct> {

    protected Comparator<PackageInfoStruct> byMatch = new Comparator<PackageInfoStruct>() {
        @Override
        public int compare(PackageInfoStruct o1, PackageInfoStruct o2) {
            if(o1.match == null){
                if(o2.match == null){
                    return 0;
                } else {
                    return - o2.match.compareTo(o1.match);
                }
            } else {
                return o1.match.compareTo(o2.match);
            }
        }
    };

    protected Comparator<PackageInfoStruct> byLabel = new Comparator<PackageInfoStruct>() {
        @Override
        public int compare(PackageInfoStruct o1, PackageInfoStruct o2) {
            return o1.label.toString().compareTo(o2.label.toString());
        }
    };

    protected Comparator<PackageInfoStruct> byUsage = new Comparator<PackageInfoStruct>() {
        @Override
        public int compare(PackageInfoStruct o1, PackageInfoStruct o2) {
            return o1.usageCounter - o2.usageCounter;
        }
    };


    @Override
    public int compare(PackageInfoStruct o1, PackageInfoStruct o2) {
        return byMatch.reversed()
                .thenComparing(byUsage.reversed())
                .thenComparing(byLabel).compare(o1, o2);
    }
}
