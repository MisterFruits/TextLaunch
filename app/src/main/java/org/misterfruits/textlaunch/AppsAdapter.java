package org.misterfruits.textlaunch;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tolstykh.textviewrichdrawable.TextViewRichDrawable;

import java.util.List;

public class AppsAdapter extends RecyclerView.Adapter<AppsAdapter.ItemViewHolder> {
    private final List<PackageInfoStruct> items;
    private final SearchActivity searchActivity;

    public AppsAdapter(List<PackageInfoStruct> items, SearchActivity searchActivity) {
        this.items = items;
        this.searchActivity = searchActivity;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        ItemViewHolder result = new ItemViewHolder(itemView);
        itemView.setOnClickListener(result);
        itemView.setOnLongClickListener(result);
        return result;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        View view = holder.view;
        PackageInfoStruct item = this.items.get(position);
        TextViewRichDrawable itemView = view.findViewById(R.id.item_text_view);
        itemView.setText(item.label);
        itemView.setDrawables(null, item.icon, null , null);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        View view;

        public ItemViewHolder(View itemView) {
            super(itemView);
            view = itemView.findViewById(R.id.item_layout);
        }

        @Override
        public void onClick(View view) {
            searchActivity.onClickItem(items.get(getAdapterPosition()));
        }

        @Override
        public boolean onLongClick(View v) {
            searchActivity.onLongClickItem(items.get(getAdapterPosition()), v);
            return true;
        }
    }
}
