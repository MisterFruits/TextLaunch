package org.misterfruits.textlaunch;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

public class MyPreferenceActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyPreferenceFragment fragment = new MyPreferenceFragment();
        fragment.setPackageUsageCounter(this.getSharedPreferences(getString(R.string.package_usage_counter_file), Context.MODE_PRIVATE));
        getFragmentManager().beginTransaction().replace(android.R.id.content, fragment).commit();
    }

    public static class MyPreferenceFragment extends PreferenceFragment
    {
        SharedPreferences packageUsageCounter;

        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
            Preference button = findPreference("reset_app_counter_button");
            button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    MyPreferenceFragment.this.packageUsageCounter.edit().clear().apply();
                    return true;
                }
            });
        }

        public void setPackageUsageCounter(SharedPreferences sharedPreferences) {
            this.packageUsageCounter = sharedPreferences;
        }
    }

}