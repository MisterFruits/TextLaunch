# [unreleased]

## Feature

## Bugfix

 - Broken web search - #46

## Project maintenance

# [v0.5] - 2020-04-30

## Feature

 - Allow app uninstall from launcher - #30
 - Add a setting panel - #8
 - Add application data and settings auto-backup - #37

## Project maintenance

 - Improve contribution guide by faking a new contibution - #24
 - Replace item_layout by a TextView with compound drawables - #36

# [v0.4] - 2020-01-03

## Feature

 - Sort app using usage ranking - #28
 - Numerical expression evaluation - #20
 - Actualize app list upon app un/install - #16

## Bugfix

 - Trying to open an uninstalled app results in a crash - #16

## Project maintenance

 - Sign CI-generated apk - #23
 - Use RecyclerView - #21
 - Rename lauch into launch - #34

# [v0.3] - 2019-06-10

## Feature

 - Style search bar with card theme - #10
 - Implement fuzzy search ranking from ST - #7 see #29 for any question
 - Enable search engine query directly from the TextLaunch searchbar - #19
 - Make background transparent to show system wallpaper - #11

## Project maintenance

 - Gather build reports in CI pipelines - #17


# [v0.2] - 2018-12-29

## Feature

 - Clean search field when an application is launched - #9

## Project maintenance

 - Improve README with contributing, screenshots, quick starts - #5
 - Setup templates for issues and merge requests - #3
 - Setup test environment and first tests - #2


# [v0.1] - 2018-12-19

## Feature

 - Sort apps alphabetically - #12
 - Launch app when tapped on - #13

## Project maintenance

 - Setup Gitlab CI - #1
